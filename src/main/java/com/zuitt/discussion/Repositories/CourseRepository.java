package com.zuitt.discussion.Repositories;

import com.zuitt.discussion.models.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course,Object> {
}
